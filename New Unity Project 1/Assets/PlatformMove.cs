﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platformy : MonoBehaviour
{
    public bool MoveX;
    public bool MoveY;
    public bool MoveZ;
    public float MoveStepX;
    public float MoveStepY;
    public float MoveStepZ;
    float NapryamX, NapryamY, NapryamZ = 0;
    public float OffSetX, OffSetY, OffSetZ = 0;
    public float MaxOffSetX, MaxOffSetY, MaxOffSetZ;

    Transform StartPositions;
    Transform TargetPositions;


    void Start()
    {

        StartPositions = this.gameObject.transform;



    }

    void Update()
    {
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetX > 0)
            NapryamX = -1;

        OffSetX = OffSetX + (MoveStepX * NapryamX);
    }
}