﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {
    int BallJumpPower = 10000;
    int BallMovePower = 100;
    public Rigidbody Ball;

    void Start() {
        Ball = this.gameObject.GetComponent<Rigidbody>();
    }


    void Update() {
        if (Input.GetKeyDown(KeyCode.Space))
            Ball.AddForce(Vector3.up * BallJumpPower);
        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.forward * BallMovePower);
        if (Input.GetKey(KeyCode.A))
            Ball.AddForce(Vector3.left * BallMovePower);
        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallMovePower);
        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallMovePower);
    }
}
